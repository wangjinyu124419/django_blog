import logging
import logging.handlers

def setup_logger(stream=True,log_file=None, level=logging.DEBUG):
    # logger = logging.getLogger(name)
    # logger.setLevel(level)


    logging.getLogger().setLevel(level)
    # handler = logging.FileHandler(log_file)

    handler = logging.handlers.RotatingFileHandler(
        log_file,
        mode='a',
        maxBytes=50*1024*1024,
        backupCount=10,
        encoding='utf-8'
    )
    formatter = logging.Formatter('%(asctime)s-%(name)s-%(levelname)s-%(funcName)s -%(lineno)d - %(message)s')
    handler.setFormatter(formatter)

    logging.getLogger().addHandler(handler)

    if stream:
        stream_handler = logging.StreamHandler()
        stream_handler.setFormatter(formatter)
        stream_handler.setLevel(level)
        logging.getLogger().addHandler(stream_handler)