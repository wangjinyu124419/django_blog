from django.urls import path

# 正在部署的应用的名称
app_name = 'articles'

from . import views
# from articles import views
urlpatterns = [
    #博客主页
    path('', views.articles_list, name='home'),
    #文章列表
    path('articles', views.articles_list, name='articles_list'),
    #文章详情
    path('article_detail/<int:id>/', views.article_detail, name='article_detail'),
    #创建文章
    path('article_create/', views.article_create, name='article_create'),
    #删除文章
    path('article_delete/<int:id>/', views.article_delete, name='article_delete'),
    #修改文章
    path('article_update/<int:id>/', views.article_update, name='article_update'),

]


