from django.urls import path
from . import views

app_name = 'users'

urlpatterns = [
    # 用户登录
    path('login/', views.user_login, name='user_login'),
    path('logout/', views.user_logout, name='user_logout'),
    path('register/', views.user_register, name='register'),
    path('delete/<int:id>/', views.user_delete, name='delete'),
]