import logging
import json

from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse
from .users_form import UserLoginForm, UserRegisterForm
# 引入验证登录的装饰器
from django.contrib.auth.decorators import login_required


# Create your views here.
logger=logging.getLogger(__name__)


@login_required(login_url='/user/login/')
def user_delete(request, id):
    user = User.objects.get(id=id)
    # 验证登录用户、待删除用户是否相同
    if request.user == user:
        #退出登录，删除数据并返回博客列表
        logout(request)
        user.delete()
        return redirect("articles:articles_list")
    else:
        return HttpResponse("你没有删除操作的权限。")

def user_register(request):
    if request.method == 'POST':
        logger.debug('准备提交表单')
        user_register_form = UserRegisterForm(data=request.POST)
        # logger.debug(request.POST)
        # logger.debug(user_register_form)
        # logger.debug(user_register_form.username)
        # logger.debug(user_register_form.repeat_password)
        # logger.debug(user_register_form.password)

        if user_register_form.is_valid():
            logger.debug('验证通过')
            new_user = user_register_form.save(commit=False)
            # 设置密码
            new_user.set_password(user_register_form.cleaned_data['password'])
            new_user.save()
            # 保存好数据后立即登录并返回博客列表页面
            login(request, new_user)
            return redirect("articles:articles_list")
        else:
            #取出验证失败的信息,方便定位问题,给用用户提示
            ErrorDict=user_register_form.errors

            Error_Str=json.dumps(ErrorDict)


            Error_Dict=json.loads(Error_Str)

            # for value in  Error_Dict.values():
            #     logger.debug(value)
            # return HttpResponse(ErrorDict.__repr__())
            return HttpResponse(Error_Dict.values())
    elif request.method == 'GET':
        logger.debug('开始注册')
        user_register_form = UserRegisterForm()
        context = { 'form': user_register_form }
        return render(request, 'users/register.html', context)
    else:
        return HttpResponse("请使用GET或POST请求数据")


# 用户退出
def user_logout(request):
    data = request.POST
    logger.debug('用户：%r退出成功'%data)

    logout(request)

    return redirect("articles:articles_list")


#用户登录
def user_login(request):
    if request.method == 'POST':
        user_login_form = UserLoginForm(data=request.POST)
        data=request.POST

        logger.debug('用户：%r正在登录' %data)
        logger.debug('用户：%r正在登录' %data.get('username') )

        if user_login_form.is_valid():
            # .cleaned_data 清洗出合法数据
            data = user_login_form.cleaned_data
            # 检验账号、密码是否正确匹配数据库中的某个用户
            # 如果均匹配则返回这个 user 对象
            user = authenticate(username=data['username'], password=data['password'])

            if user:
                # 将用户数据保存在 session 中，即实现了登录动作
                login(request, user)

                logger.debug('用户：%r登录成功' % (user.username))

                return redirect("articles:articles_list")
            else:
                return HttpResponse("账号或密码输入有误。请重新输入~")
        else:
            return HttpResponse("账号或密码输入不合法")
    elif request.method == 'GET':
        user_login_form = UserLoginForm()
        context = { 'form': user_login_form }
        return render(request, 'users/login.html', context)
    else:
        return HttpResponse("请使用GET或POST请求数据")