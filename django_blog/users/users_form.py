import logging
# 引入表单类
from django import forms
# 引入 User 模型
from django.contrib.auth.models import User



from .models import Profile


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('phone', 'avatar', 'bio')

# 登录表单，继承了 forms.Form 类
class UserLoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField()


logger = logging.getLogger(__name__)


# 注册用户表单
class UserRegisterForm(forms.ModelForm):
    # 复写 User 的密码
    password = forms.CharField()
    repeat_password = forms.CharField()

    class Meta:
        model = User
        fields = ('username', 'email')

    # 对两次输入的密码是否一致进行检查
    """
    验证密码一致性方法不能写def clean_password()，因为如果你不定义def clean_repeat_password()方法，会导致repeat_password中的数据被Django判定为无效数据从而清洗掉，从而repeat_password属性不存在。最终导致两次密码输入始终会不一致，并且很难判断出错误原因。
    def clean_[字段]这种写法Django会自动调用，来对单个字段的数据进行验证清洗。
    """

    # 这种写法导致repeat_password属性不存在
    # def clean_password(self):

    def clean_repeat_password(self):

        logger.debug('调用clean_repeat_password')
        data = self.cleaned_data
        logger.debug('first_password:%r'%data.get('password'))
        logger.debug('repeat_password:%r'%data.get('repeat_password'))
        if data.get('password') == data.get('repeat_password'):
            logger.debug(data.get('password'))
            return data.get('password')
        else:
            logger.debug('密码输入不一致')
            raise forms.ValidationError("密码输入不一致,请重试。")
